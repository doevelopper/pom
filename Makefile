.DEFAULT_GOAL:=help

ifeq ("$(origin V)", "command line")
    KBUILD_VERBOSE = $(V)
endif
ifndef KBUILD_VERBOSE
    KBUILD_VERBOSE = 0
endif

ifeq ($(KBUILD_VERBOSE),1)
    quiet =
    Q =
    QQ =
else
    quiet=quiet_
    Q = @
    QQ = > /dev/null
endif

ifneq ($(findstring s,$(filter-out --%,$(MAKEFLAGS))),)
    quiet=silent_
    tools_silent=s
endif

ifeq (0,${MAKELEVEL})
	whoami    := $(shell whoami)
	host-type := $(shell arch)
	# MAKE := ${MAKE} host-type=${host-type} whoami=${whoami}
endif

MAKEFLAGS += --no-print-directory

export BUILD_DIRECTORY ?= $(shell basename $(shell git rev-parse --show-toplevel))-build
export PRJNAME := $(shell basename $(shell git rev-parse --show-toplevel))
export BRANCH := $(shell git rev-parse --abbrev-ref HEAD)
export HASH := $(shell git rev-parse HEAD)


ifeq ($(BRANCH),master)
    RELEASE_LEVEL := "CANDIDATE"
else ifeq ($(BRANCH),develop)
    RELEASE_LEVEL := "ALPHA"
else ifeq ($(BRANCH),release)
    RELEASE_LEVEL := "FINAL"
else ifeq ($(BRANCH),hotfix)
    RELEASE_LEVEL := "HOTFIX"
else ifeq ($(BRANCH),support)
    RELEASE_LEVEL := "HOTFIX"
else ifeq ($(BRANCH),feature)
    RELEASE_LEVEL := "SNAPSHOOT"
else
    RELEASE_LEVEL := ""
endif

SHELL = /bin/sh
RM = /opt/bin/cmake -E remove -f

export BAZEL_BIN=$(bazel info bazel-bin)
export BAZEL_OUTPUT_BASE=$(bazel info output_base)
export BAZEL_SERVER_PID=$(bazel info server_pid)
export BAZEL_TESTLOGS=$(bazel info bazel-testlogs)
export BAZEL_GENFILES=$(bazel info bazel-genfiles)
export BAZEL_EXTERNAL=$(bazel info output_base)/external
PATH  := $(PATH):$(PWD)/bin:/my/other/path
.PHONY: format-build
format-build: ##  create standardized formatting for BUILD and .bzl
	@bazel buildifier $(find . -type f \( -iname BUILD -or -iname BUILD.bazel \))
	@bazel run @com_github_bazelbuild_buildtools//:buildifier

.PHONY: build-deps
build-deps: ##  Build sw external deps
	@bazel --output_user_root=/tmp/bazel_output_root build  @com_google_crc32c//:crc32c
	@bazel --output_user_root=/tmp/bazel_output_root build  @com_github_google_benchmark//:benchmark
	@bazel --output_user_root=/tmp/bazel_output_root build  @com_github_tencent_rapidjson//:rapidjson
	@bazel --output_user_root=/tmp/bazel_output_root build  @zlib//:zlib
	@bazel query @com_google_double_conversion//...
	@bazel --output_user_root=/tmp/bazel_output_root build  @com_google_double_conversion//...
	@bazel build  @com_google_statechart//statechart:state_machine
	@bazel build  @com_googlesource_code_re2//:re2
	@bazel query @com_google_protobuf//...
	@bazel build  @com_google_protobuf//:protoc
	@bazel run @com_google_protobuf//:protoc -- --help

## for all targets in src/main/cpp ind all dependencies (a transitive closure set), then tell me which ones depend on the gtest
## target in the root package of the external workspace com_google_googletest
## Bazel reports this reverse dependency set. We request the output in graphviz format and pipe this to dot to generate the figure:

.PHONY: querygrpcdeps
querygrpcdeps: ## for all targets in src/main/cpp find all dependencies (a transitive closure set), then tell me which ones depend on the gtest
	@bazel query "rdeps(deps(//src/...), "@com_google_googletest//:gtest")" --output graph | dot -Tpng -O

.PHONY: querybin
querybin: ## List binary target
	@bazel query 'kind(cc_binary, //...)'

.PHONY: main-compile
main-compile: ## Build all xcept Test target rules
	@bazel query '//... except kind(.*test, //...)' | xargs bazel build 

.PHONY: pi-compile
pi-compile: ## Build all armv8l: processor architecture used in the Raspberry Pi family of embedded products.
	@bazel query '//... except kind(.*test, //...)' | xargs bazel build --config=armv8l

.PHONY: jetson-compile
jetson-compile: ## Build all aarch64: processor architecture used in Jetson TX1 TX2 Xavier and Nano products.
	@bazel query '//... except kind(.*test, //...)' | xargs bazel build --config=aarch64

.PHONY: querybuild
querybuild: ## List buildable targets
	@bazel query --keep_going --noshow_progress "kind(.*_binary, rdeps(//..., set(${files[*]})))"

.PHONY: queryall
queryall: ## List all targets
	@bazel query @bazel_tools//src/conditions:all
	@bazel query //...
	@bazel query //src/... --output label_kind | sort | column -t

.PHONY: compile
compile: ## Build projects main sources
	@bazel build  //... --action_env=RELEASE=${RELEASE_LEVEL}

.PHONY: test
test: ## Build projects test sources and run unit test
	PATH=$$(bazel info output_base):$$PATH
	@bazel test //... --test_output=all --test_env=PATH="$${PATH}:$$(bazel info output_base)/external/org_ruby_lang_ruby_toolchain/build/bin" --test_env=LOG4CXX_CONFIGURATION=${PWD}/src/main/resources/configs/log4cxx.xml

.PHONY: coverage
coverage:  ## Generates code coverage report
	# NOK @bazel coverage  --instrument_test_target --combined_report=lcov --coverage_report_generator=@bazel_tools//tools/test/CoverageOutputGenerator/java/com/google/devtools/coverageoutputgenerator:Main //...
	@bazel coverage -s --combined_report=lcov --instrumentation_filter=//src/... --coverage_report_generator=@bazel_tools//tools/test:coverage_report_generator  //src/...

# ./bazelw query "attr(name, '.*test_main', //...)" | xargs ./bazelw coverage --combined_report=lcov

.PHONY: sonar-scanner
sonar-scanner:  ## Code Quality & Code Security
	@bazel clean
	@build-wrapper-linux-x86-64 --out-dir sonar-dir bazel --batch build --spawn_strategy=standalone --genrule_strategy=standalone  //...

.PHONY: genhtml
genhtml: coverage  ## Generate HTML view from LCOV coverage data files
	@rm -rf coverage_report
	@mkdir coverage_report
	@genhtml bazel-out/_coverage/_coverage_report.dat --output-directory coverage_report/
	@firefox ./coverage_report/index.html > /dev/null 2>&1 &

.PHONY: clean
clean: ## Cleaned up the objects and intermediary files
	@bazel clean

.PHONY: expunge
expunge: ## Removes the entire working tree for this bazel instance
	@bazel clean --expunge

.PHONY: all
all: compile   ## Build test , regression test , coverage qnd documentations

.PHONY: help
help: ## Display this help and exits.
	$(Q)echo "$@ ->"
	$(Q)echo '---------------$(CURDIR)------------------'
	$(Q)echo '+----------------------------------------------------------------------+'
	$(Q)echo '|                        Available Commands                            |'
	$(Q)echo '+----------------------------------------------------------------------+'
	$(Q)echo
	$(Q)awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {sub("\\\\n",sprintf("\n%22c"," "), $$2);printf " \033[36m%-20s\033[0m  %s\n", $$1, $$2}' $(MAKEFILE_LIST)
	$(Q)echo ""
	$(Q)echo
