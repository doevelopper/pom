
#ifndef PROJECT_OBJECT_MODEL_UTILS_GITREVISION_HPP
#define PROJECT_OBJECT_MODEL_UTILS_GITREVISION_HPP
    #define HASH                       ""
    #define DATE                       ""
    #define BRANCH                     "GIT_BRANCH"
    #define GIT_COMMIT_VERSION         0x
    #define GIT_COMMIT_DESC            ""
    #define NB_GIT_COMMIT              ""
    #define ALL_GIT_COMMIT_COUNT       ""
    #define CMAKE_COMMAND              R"(/opt/cmake/bin/cmake) (3.17.0)"
    #define BUILD_TYPE                 ""
    #define BUILD_PROCESSOR            "x86_64"
    #define SOURCE_DIRECTORY           R"(/home/happyman/cfs-platform/cfs-osal)"
    #define BUILD_DIRECTORY            R"()"
    #define VER_COMPANYNAME_STR        "A.H.L SW Engineering"
    #define VER_LEGALCOPYRIGHT_STR     "(c) 2015- A.H.L SW Engineering Labs"
    #define VER_FILEVERSION            0.0 .1.
    #define HOST_OS_NAME               "Linux"
    #define HOST_OS_LONGNAME           "Linux-4.15.0-99-generic"
    #define HOST_OS_VERSION            "4.15.0-99-generic"
    #define VER_FILEVERSION_STR        " "
    #define VER_PRODUCTVERSION          VER_FILEVERSION
    #define VER_PRODUCTVERSION_STR      VER_FILEVERSION_STR
#endif
