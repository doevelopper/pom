package com.project.object.model.test_doubles.dummy;

public interface EmailService {
	void sendEmail(String message);
}
