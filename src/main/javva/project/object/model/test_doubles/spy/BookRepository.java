package com.project.object.model.test_doubles.spy;

import java.util.Collection;

public interface BookRepository {
	void save(Book book);
}
