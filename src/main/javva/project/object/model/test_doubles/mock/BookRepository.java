package com.project.object.model.test_doubles.mock;

public interface BookRepository {
	void save(Book book);
}
