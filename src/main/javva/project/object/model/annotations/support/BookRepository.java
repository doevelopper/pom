package com.project.object.model.annotations.support;

import java.util.List;

public interface BookRepository {
	List<Book> findNewBooks(int days);
}
