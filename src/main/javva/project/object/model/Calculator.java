package com.project.object.model;

public class Calculator {

	public int add(int a, int b) {
		return a + b;
	}

	public boolean isEvenNumber(int number) {
		return number % 2 == 0;
	}
}