package com.project.object.model.behavior.verification;

import java.util.List;

public interface BookRepository {
	void save(Book book);
	Book findBookById(String bookId);
}
