# This file exists so that visibility rules can be relaxed for open source
# clients of kythe, but more restrictive for use within google. Targets that are
# allowed to be used by kythe clients should be marked as "//visibility:public",
# but separate aliases are used so they can map to different package groups
# within google.

"""
.
"""
PUBLIC_PROTO_VISIBILITY = "//visibility:public"

PUBLIC_VISIBILITY = "//visibility:public"

#PUBLIC_VISIBILITY = "//visibility:public"

# https://gcc.gnu.org/wiki/Visibility

SHARED_LIBRARY_SUPPORT = select({
    "//config:windows": [
        "-DHELPER_DLL_IMPORT=__declspec(dllimport)",
        "-DHELPER_DLL_EXPORT=__declspec(dllexport)",
        "-DHELPER_DLL_LOCAL=",
    ],
    "//config:linux": [
        "-DHELPER_DLL_IMPORT=__attribute__ ((visibility (\"default\")))",
        "-DHELPER_DLL_EXPORT=__attribute__ ((visibility (\"default\")))",
        "-DHELPER_DLL_LOCAL=__attribute__ ((visibility (\"hidden\")))",
    ],
    "//conditions:default": [
        "-DHELPER_DLL_IMPORT",
        "-DHELPER_DLL_EXPORT",
        "-DHELPER_DLL_LOCAL",
    ],
})

# GENERIC_HELPER_DEFINITIONS= select({
#     ":USE_DLL": [
#         "-DAPI_EXPORT=HELPER_DLL_EXPORT",
#         "-DAPI_LOCAL=DHELPER_DLL_LOCAL",
#     ],
#     "//config:windows": [
#     ],
#     "//config:windows": [
#     ],
# })
#API_EXPORT =
#API_LOCAL_EXPORT =