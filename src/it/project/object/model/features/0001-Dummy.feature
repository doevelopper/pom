# language: en
# Project cfs-platform
# file 001-Dummy.feature
#

Feature: Dummy Feature
    As a developer starting a new project
    I want to have a dummy feature
    In order to get started and now that the Behavior Driven Development toolchain works

    Background: Adding a background won't make a pickle
    #        * a step

    Scenario: no steps

    Scenario: Make Sure Cucumber Works
    #        Given I execute dummy
    #          And I do nothing
    #         Then I should see some output
    #         Then Success

    Scenario: Some determinable business situation
    #     Given some precondition
    #       And some other precondition
    #      When some action by the actor
    #       And some other action
    #       And yet another action
    #      Then some testable outcome is achieved
    #       And something else we can check happens too

    # Scenario Outline: Single dummy says Hello World
    #     Given  a dummy initialised with <hello> and <world>
    #      When  I command the dummy to say hello
    #      Then  The dummy should say <hello world>

    #     Examples:
    #         | hello     | world           | hello world             |
    #         | "Hello"   | "World"         | "Hello World"           |
    #         | "Hallo"   | "Welt"          | "Hallo Welt"            |
    #         | "Bonjour" | "tout le monde" | "Bonjour tout le monde" |
    #         | "Hola"    | "mundo"         | "Hola mundo"            |
    #         | "Hello"   | "vilag"         | "Hello vilag"           |


    # Scenario Outline: Multiple dummies say Hello World
    #     Given the following dummies:
    #         | hello   | world         |
    #         | Hello   | World         |
    #         | Hallo   | Welt          |
    #         | Bonjour | tout le monde |
    #         | Hola    | mundo         |
    #         | Hello   | vilag         |

    #     When I command the dummy <index> to say hello
    #     Then the dummy should say <hello world>

    #     Examples:
    #         | index | hello world             |
    #         | 0     | "Hello World"           |
    #         | 1     | "Hallo Welt"            |
    #         | 2     | "Bonjour tout le monde" |
    #         | 3     | "Hola mundo"            |
    #         | 4     | "Hello vilag"           |
