#           Copyright (C) 2021 Adrien H.
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.


""" Workspace for a template of a software project """

workspace(name = "com.gitlab.project.object.model")

BEHIND_PROXY_PLACE_HOLDER = "file://" + __workspace_dir__ + "/src/main/resources/place_holder_4_proxy/"
JUNIT_JUPITER_VERSION = "5.7.1"
JUNIT_PLATFORM_VERSION = "1.7.1"

load("@com.gitlab.project.object.model//src/main/resources/repos:sw_dev.bzl", "dev_repositories")
dev_repositories()

load("@rules_pkg//:deps.bzl", "rules_pkg_dependencies")
rules_pkg_dependencies()

load("@com.gitlab.project.object.model//src/main/resources/repos:sw_qa.bzl", "qa_repositories")
qa_repositories()

load("@com.gitlab.project.object.model//src/main/resources/repos:junit5.bzl",  "junit_jupiter_java_repositories", "junit_platform_java_repositories")

junit_jupiter_java_repositories(
    version = JUNIT_JUPITER_VERSION,
)

junit_platform_java_repositories(
    version = JUNIT_PLATFORM_VERSION,
)


load("@com.gitlab.project.object.model//src/main/resources/repos:dependencies.bzl", "rules_sdlc_dependencies")
rules_sdlc_dependencies()

load("@rules_cc//cc:repositories.bzl", "rules_cc_dependencies")
rules_cc_dependencies()

load("@rules_proto_grpc//:repositories.bzl", "rules_proto_grpc_repos", "rules_proto_grpc_toolchains")
rules_proto_grpc_toolchains()
rules_proto_grpc_repos()

load("@com_github_nelhage_rules_boost//:boost/boost.bzl", "boost_deps")
boost_deps()

# load("@rules_java//java:repositories.bzl", "rules_java_dependencies", "rules_java_toolchains")
# rules_java_dependencies()
# rules_java_toolchains()

load("@rules_proto//proto:repositories.bzl", "rules_proto_dependencies", "rules_proto_toolchains")
rules_proto_dependencies()
rules_proto_toolchains()

load("@com_github_grpc_grpc//bazel:grpc_deps.bzl", "grpc_deps")
grpc_deps()

load("@com_github_grpc_grpc//bazel:grpc_extra_deps.bzl", "grpc_extra_deps")
grpc_extra_deps()

# load("@com_github_brpc_brpc//bazel:workspace.bzl", "brpc_workspace")
# brpc_workspace()

# load("@com_github_brpc_brpc//:bazel/brpc.bzl", "brpc_proto_library")
#brpc_proto_library()

# load("@bazel_toolchains//rules:rbe_repo.bzl", "rbe_autoconfig")
# rbe_autoconfig(name = "rbe_default")

load("@rules_foreign_cc//:workspace_definitions.bzl", "rules_foreign_cc_dependencies")
rules_foreign_cc_dependencies()

load("@com_github_google_rules_install//:deps.bzl", "install_rules_dependencies")
install_rules_dependencies()

load("@com_github_google_rules_install//:setup.bzl", "install_rules_setup")
install_rules_setup()


###  BDD Integration test  
####   as dependency sudo apt-get install -y libssl-dev libreadline-dev
# load("@com_grail_bazel_toolchain//toolchain:deps.bzl", "bazel_toolchain_dependencies")
# bazel_toolchain_dependencies()

# load("@bazelruby_rules_ruby//ruby:deps.bzl","rules_ruby_dependencies","rules_ruby_select_sdk",)
# rules_ruby_dependencies()

# load("@bazel_skylib//:workspace.bzl", "bazel_skylib_workspace")
# bazel_skylib_workspace()
# rules_ruby_select_sdk(version = "2.6.3")

# load("@bazelruby_rules_ruby//ruby:defs.bzl", "ruby_bundle",)

load("//src/main/resources/repos:midlware.bzl", "distributed_applications_repositories")
distributed_applications_repositories()

load("@rules_gherkin//:gherkin_deps.bzl","gherkin_deps")
gherkin_deps()

load("@rules_gherkin//:gherkin_workspace.bzl","gherkin_workspace")
gherkin_workspace()